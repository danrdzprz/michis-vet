<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Office;

class OfficeSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $department = new Office;
    $department->name = 'Consultorio A';
    $department->address = 'Calle 1';
    $department->save();

    $department = new Office;
    $department->name = 'Consultorio B';
    $department->address = 'Calle 2';
    $department->save();

    $department = new Office;
    $department->name = 'Consultorio C';
    $department->address = 'Calle 3';
    $department->save();
  }
}
