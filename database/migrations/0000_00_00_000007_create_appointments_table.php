<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppointmentsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
  Schema::create('appointments', function (Blueprint $table) {
    $table->id();
    $table->string('description');
    $table->unsignedBigInteger('pet_id');
    $table->datetime('start_date');
    $table->datetime('end_date');
    $table->unsignedBigInteger('veterinary_id');
    $table->foreign('veterinary_id')->references('id')->on('veterinaries')->onDelete('cascade');
    $table->foreign('pet_id')->references('id')->on('pets')->onDelete('cascade');
    $table->softDeletes();
    $table->timestamps();
  });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('appointments', function (Blueprint $table) {
      $table->dropSoftDeletes();
    });
    Schema::dropIfExists('appointments');
  }
}
