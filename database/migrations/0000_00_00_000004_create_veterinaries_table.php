<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVeterinariesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
  Schema::create('veterinaries', function (Blueprint $table) {
    $table->id();
    $table->string('name');
    $table->string('last_name')->default(null)->nullable();
    $table->string('email')->unique();
    $table->unsignedBigInteger('office_id');
    $table->string('phone');
    $table->string('rfc');
    $table->string('curp');
    $table->enum('gender',['male', 'female']);
    $table->foreign('office_id')->references('id')->on('offices')->onDelete('cascade');
    $table->softDeletes();
    $table->timestamps();
  });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('veterinaries', function (Blueprint $table) {
      $table->dropSoftDeletes();
    });
    Schema::dropIfExists('veterinaries');
  }
}
