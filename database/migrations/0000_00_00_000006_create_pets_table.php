<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePetsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('pets', function (Blueprint $table) {
      $table->id();
      $table->integer('age');
      $table->enum('kind',['cat','dog','bird','rodent'])->default('cat');
      $table->string('name');
      $table->unsignedBigInteger('owner_id');
      $table->enum('gender',['male','female'])->default('male');
      $table->decimal('weight', 7, 2);
      $table->foreign('owner_id')->references('id')->on('owners')->onDelete('cascade');
      $table->softDeletes();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('pets', function (Blueprint $table) {
      $table->dropSoftDeletes();
    });
    Schema::dropIfExists('pets');
  }
}
