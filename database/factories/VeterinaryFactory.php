<?php

namespace Database\Factories;

use App\Models\Veterinary;
use App\Models\Office;
use Illuminate\Database\Eloquent\Factories\Factory;

class VeterinaryFactory extends Factory
{
  /**
   * The name of the factory's corresponding model.
   *
   * @var string
   */
  protected $model = Veterinary::class;

  /**
   * Define the model's default state.
   *
   * @return array
   */
  public function definition()
  {
    return [
      'name' => $this->faker->name,
      'last_name' => $this->faker->lastName,
      'email' => $this->faker->unique()->safeEmail,
      'phone' => $this->faker->phoneNumber,
      'curp' => $this->faker->regexify('[A-Z]{1}[AEIOU]{1}[A-Z]{2}[0-9]{2}(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[HM]{1}(AS|BC|BS|CC|CS|CH|CL|CM|DF|DG|GT|GR|HG|JC|MC|MN|MS|NT|NL|OC|PL|QT|QR|SP|SL|SR|TC|TS|TL|VZ|YN|ZS|NE)[B-DF-HJ-NP-TV-Z]{3}[0-9A-Z]{1}[0-9]{1}'),
      'rfc' => $this->faker->regexify('[A-Z]{4}([0-9]{2})(1[0-2]|0[1-9])([0-3][0-9])([ -]?)([A-Z0-9]{4})'),
      'office_id'=>Office::inRandomOrder()->value('id'),
      'gender' => $this->faker->randomElement(['male', 'female']),
    ];
  }
}
