<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\OwnerController;
use App\Http\Controllers\PetController;
use App\Http\Controllers\AppointmentController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Group owner
Route::group(['prefix' => 'owner'], function(){
  // GET api/onwer
  Route::get('/', [OwnerController::class, 'index'])->name('api.owner.index');
  // POST api/onwer
  Route::post('/', [OwnerController::class, 'store'])->name('api.owner.store');
  // GET api/onwer
  Route::put('{owner}', [OwnerController::class, 'update'])->name('api.owner.update');
  // GET api/onwer
  Route::delete('{owner}', [OwnerController::class, 'destroy'])->name('api.owner.destroy');
  // GET api/onwer
  Route::get('{owner}', [OwnerController::class, 'show'])->name('api.owner.show');
  //Group pet
  Route::group(['prefix' => '{owner}/pet'], function(){
    // GET api/onwer/{owner}/pet
    Route::get('/', [PetController::class, 'index'])->name('api.pet.index');
    // POST api/onwer/{owner}/pet
    Route::post('/', [PetController::class, 'store'])->name('api.pet.store');
    // GET api/onwer/{owner}/pet/{pet}
    Route::put('{pet}', [PetController::class, 'update'])->name('api.pet.update');
    // GET api/onwer/{owner}/pet{pet}
    Route::delete('{pet}', [PetController::class, 'destroy'])->name('api.pet.destroy');
    // GET api/onwer/{owner}/pet{pet}
    Route::get('{pet}', [PetController::class, 'show'])->name('api.pet.show');
  });
});

//Group appointment
Route::group(['prefix' => 'appointment'], function(){
  // GET api/appointment
  Route::get('/', [AppointmentController::class, 'index'])->name('api.appointment.index');
  // POST api/appointment
  Route::post('/', [AppointmentController::class, 'store'])->name('api.appointment.store');
  // GET api/appointment
  Route::get('busiest_vet', [AppointmentController::class, 'busiestVet'])->name('api.appointment.busiest_vet');
  // GET api/appointment/{appointment}
  Route::get('{appointment}', [AppointmentController::class, 'show'])->name('api.appointment.show');
});