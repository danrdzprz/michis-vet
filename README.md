## About Laravel
## Proyecto en laravel 8.0

## requisitos 
- **[Composer]**
- **[Phpv7.4]**
- **[Mysql]**

0. Ejecutar en consola al nivel raiz del proyecto "composer install"
1. Crear duplicado del archivo env.example y renombrarlo como .env
2. Crear base de datos "michis_vet" y definirla en el archivo de entorno .env
3. Ejecutar en consola al nivel raiz del proyecto "php artisan key:generate"
4. Ejecutar en consola al nivel raiz del proyecto "php artisan migrate:fresh --seed"
