<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OwnerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
      return [
        'id' => $this->id,
        'name' => $this->name,
        'last_name' => $this->last_name,
        'email' => $this->email,
        'phone' => $this->phone,
        'rfc' => $this->phone,
        'curp' => $this->phone,
        'pets'=> PetResource::collection($this->pets)
      ];
    }
}
