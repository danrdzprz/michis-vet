<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class VeterinaryResource extends JsonResource
{
  /**
   * Transform the resource into an array.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return array
   */
  public function toArray($request)
  {
    return [
      'name'=>$this->name,
      'last_name'=>$this->last_name,
      'email'=>$this->email,
      'phone'=>$this->phone,
      'rfc'=>$this->rfc,
      'curp'=>$this->curp
    ];
  }
}
