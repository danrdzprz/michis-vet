<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PetResource extends JsonResource
{
  /**
   * Transform the resource into an array.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return array
   */
  public function toArray($request)
  {
    return [
      'id'=>$this->id,
      'age'=>$this->age,
      'kind'=>$this->kind,
      'name'=>$this->name,
      'gender'=>$this->gender,
      'weight'=>$this->weight,
    ];
  }
}
