<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AppointmentResource extends JsonResource
{
  /**
   * Transform the resource into an array.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return array
   */
  public function toArray($request)
  {
    return [
      'id' => $this->id,
      'description' => $this->description,
      'start_date' => $this->start_date,
      'end_date' => $this->end_date,
      'pet'=> new PetResource($this->pet),
      'veterinary'=> new VeterinaryResource($this->veterinary)
    ];
  }
}
