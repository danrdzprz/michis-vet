<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pet;
use App\Models\Owner;
use App\Http\Resources\PetCollection;
use App\Http\Resources\PetResource;
use DB;

class PetController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index($owner)
  {
    return response()->json( new PetCollection(Pet::where(['owner_id'=>$owner])->get()), 200);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request,$owner)
  {
    try {
      $this->validate($request,[
        'age' => 'required|integer',
        'kind' => 'required|string|in:cat,dog,bird,rodent',
        'name' => 'required',
        'gender' => 'required|string|in:male,female',
        'weight' => 'required|numeric'
      ]);
      DB::beginTransaction();
      $owner = Owner::findOrFail($owner);
      $pet = new Pet();
      $pet->age= $request->age;
      $pet->kind= $request->kind;
      $pet->name= $request->name;
      $pet->gender= $request->gender;
      $pet->weight= $request->weight;
      $pet->owner()->associate($owner);
      $pet->save();
      DB::commit();
      return response()->json(new PetResource($pet), 200);
    } catch (\Exception $exception) {
      DB::rollBack();
      return response()->json(['message' =>$exception->getMessage(),'line'=>$exception->getLine()], 400);
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($owner,$pet)
  {
    return response()->json(new PetResource(Pet::where(['id'=>$pet,'owner_id'=>$owner])->firstOrfail()), 200);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $owner, $pet)
  {
    try {
      $this->validate($request,[
        'age' => 'required|integer',
        'kind' => 'required|string|in:cat,dog,bird,rodent',
        'name' => 'required',
        'gender' => 'required|string|in:male,female',
        'weight' => 'required|numeric'
      ]);
      DB::beginTransaction();
      $pet = Pet::where(['id'=>$pet,'owner_id'=>$owner])->firstOrFail();
      $pet->update($request->all());
      DB::commit();
      return response()->json(new PetResource($pet), 200);
    } catch (\Exception $exception) {
      DB::rollBack();
      return response()->json(['message' =>$exception->getMessage(),'line'=>$exception->getLine()], 400);
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($owner, $pet)
  {
    try {
      DB::beginTransaction();
      $pet = Pet::where(['id'=>$pet,'owner_id'=>$owner])->firstOrFail();
      $pet->delete();
      DB::commit();
      return response()->json(new PetResource($pet), 200);
    } catch (\Exception $exception) {
      DB::rollBack();
      return response()->json(['message' =>$exception->getMessage(),'line'=>$exception->getLine()], 400);
    }
  }
}
