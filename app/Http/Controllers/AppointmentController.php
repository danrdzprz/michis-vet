<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Appointment;
use App\Models\Veterinary;
use App\Http\Resources\VeterinaryCollection;
use App\Http\Resources\AppointmentCollection;
use App\Http\Resources\VeterinaryResource;
use App\Http\Resources\AppointmentResource;
use Carbon\Carbon;
use DB;

class AppointmentController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    return response()->json( new AppointmentCollection(Appointment::all()), 200);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $this->validate($request,[
      "description"=>"required|string",
      "pet_id"=>"required|integer|exists:pets,id",
      "start_date"=>"required|date_format:Y-m-d H:i:s",
      "end_date"=>"required|date_format:Y-m-d H:i:s",
      "veterinary_id"=>"required|integer|exists:veterinaries,id"
    ]);
    try {
      DB::beginTransaction();
      $appointment = Appointment::create($request->all());
      DB::commit();
      return response()->json(new AppointmentResource($appointment), 200);
    } catch (\Exception $exception) {
      DB::rollBack();
      return response()->json(['message' =>$exception->getMessage(),'line'=>$exception->getLine()], 400);
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($appointment)
  {
    $appointment = Appointment::findOrFail($appointment);
    return response()->json(new AppointmentResource($appointment), 200);
  }

  /**
   * Retrive the busiest Vet.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function busiestVet(Request $request)
  {
    $this->validate($request,[
      'month'=>'required|date_format:Y'
    ]);
    $date = Carbon::now();
    $date->setMonth($request->month);
    $end_date = $date->copy()->endOfMonth();
    $start_date = $date->firstOfMonth();
    
    $count = Appointment::whereBetween('start_date',[$start_date->toDateTimeString(),$end_date->toDateTimeString()])
    ->groupBy('veterinary_id')
    ->select('veterinary_id', \DB::raw('count(*) as total'))
    ->get();
    $veterinary = $count->where('total', $count->max('total'))->first(); 
    $veterinary_id = empty($veterinary) ? null : $veterinary->veterinary_id ;
    return response()->json(new VeterinaryResource(Veterinary::findOrFail($veterinary_id)), 200);

  }
}
