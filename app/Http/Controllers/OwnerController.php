<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Owner;
use App\Http\Resources\OwnerCollection;
use App\Http\Resources\OwnerResource;
use DB;

class OwnerController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    return response()->json( new OwnerCollection(Owner::all()), 200);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    try {
      $this->validate($request,[
        "name"=>"required|string",
        "last_name"=>"required|string",
        "email"=>"required|email|unique:owners,email",
        "phone"=>"required|numeric|digits:10",
        "rfc"=>"required",
        "curp"=>"required",
        'pets' => 'required|array|min:1',
        'pets.*.age' => 'required|integer',
        'pets.*.kind' => 'required|string|in:cat,dog,bird,rodent',
        'pets.*.name' => 'required',
        'pets.*.gender' => 'required|string|in:male,female',
        'pets.*.weight' => 'required|numeric'
      ]);
      DB::beginTransaction();
      $owner = Owner::create($request->all());
      $owner->pets()->createMAny($request->pets);
      DB::commit();
      return response()->json(new OwnerResource($owner), 200);
    } catch (\Exception $exception) {
      // throw new \Exception($exception->getMessage(), 1);
      DB::rollBack();
      return response()->json(['message' =>$exception->getMessage(),'line'=>$exception->getLine()], 400);
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($owner)
  {
    $owner = Owner::findOrFail($owner);
    return response()->json(new OwnerResource($owner), 200);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $owner)
  {
    try {
      $this->validate($request,[
        "name"=>"required|string",
        "last_name"=>"required|string",
        "email"=>"required|email|unique:owners,email,".$owner,
        "phone"=>"required|numeric|digits:10",
        "rfc"=>"required",
        "curp"=>"required"
      ]);
      DB::beginTransaction();
      $owner = Owner::findOrFail($owner);
      $owner->update($request->all());
      DB::commit();
      return response()->json(new OwnerResource($owner), 200);
    } catch (\Exception $exception) {
      DB::rollBack();
      return response()->json(['message' =>$exception->getMessage(),'line'=>$exception->getLine()], 400);
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($owner)
  {
    try {
      DB::beginTransaction();
      $owner = Owner::findOrFail($owner);
      $owner->delete();
      DB::commit();
      return response()->json(new OwnerResource($owner), 200);
    } catch (\Exception $exception) {
      DB::rollBack();
      return response()->json(['message' =>$exception->getMessage(),'line'=>$exception->getLine()], 400);
    }
  }
}
