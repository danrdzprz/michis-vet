<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pet extends Model
{
  use SoftDeletes, HasFactory;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
  */
  protected $fillable = [
    'age',
    'kind',
    'name',
    'gender',
    'weight'
  ];

  /**
   * Relationship Belongs To Plan
   */
  public function owner()
  {
    return $this->belongsTo('App\Models\Owner')->withTrashed();
  }

  /**
   * Relationship has Many Appointment
  */
  public function appointment()
  {
    return $this->hasMany('App\Models\Appointment');
  }

}
