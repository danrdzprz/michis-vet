<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Owner extends Model
{
  use SoftDeletes, HasFactory;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
  */
  protected $fillable = [
    'name',
    'last_name',
    'email',
    'phone',
    'rfc',
    'curp'
  ];
  
  /**
   * Relationship has Many pets
   */
  public function pets()
  {
    return $this->hasMany('App\Models\Pet');
  }
}
