<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Veterinary extends Model
{
  use SoftDeletes, HasFactory;
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'name',
    'last_name',
    'email',
    'phone',
    'rfc',
    'curp'
  ];

  /**
   * Relationship has Many Appointment
  */
  public function appointment()
  {
    return $this->hasMany('App\Models\Appointment');
  }


  /**
   * Relationship Belongs To Office
   */
  public function office()
  {
    return $this->belongsTo('App\Models\Office')->withTrashed();
  }
}
