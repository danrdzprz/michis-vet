<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Appointment extends Model
{
  use SoftDeletes, HasFactory;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
  */
  protected $fillable = [
    'description',
    'pet_id',
    'start_date',
    'end_date',
    'veterinary_id'
  ];

  /**
   * Relationship Belongs To Pet
   */
  public function pet()
  {
    return $this->belongsTo('App\Models\Pet')->withTrashed();
  }


  /**
   * Relationship Belongs To Veterinary
   */
  public function veterinary()
  {
    return $this->belongsTo('App\Models\Veterinary')->withTrashed();
  }


}
